import React from 'react'
import Home from "./Components/Home";
import Routing from "./Routing/Route";


const App = () => {
  return (
    <Routing>
      <Home />
    </Routing>
  );
}

export default App;
