import React from 'react'

import FooterLinks from "./FooterLinks/FooterLinks";
import FooterRightsReserved from "./FooterRightsReserved/FooterRightsReserved";


const Footer = () => {
  return (
    <div>
      <FooterLinks />
      <FooterRightsReserved />
    </div>
  )
}

export default Footer