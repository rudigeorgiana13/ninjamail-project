import React from 'react'
import {Link} from 'react-router-dom'
import iconMess from '../../../UI/Icons/Navbar/mail (image).png'
import iconText from '../../../UI/Icons/Navbar/NinjaMail.png'
import './FooterLinks.scss'

const FooterLinks = () => {
  return (
    <div className='footer-container'>
      <div className='icon-container'>
          <div>  <img className='mess-icon' src={iconMess}/>  </div>
          <div>  <img className='text-icon' src={iconText}/>  </div>
      </div>

      <div className='links-wrapper'>
        <div className='links-column'>
          <Link to='/features'> Features </Link>
          <Link to='/pricing'> Pricing </Link>
          <Link to='/services'> Services </Link>
          <Link to='/partners'> Partners </Link>
        </div>

        <div className='links-column'>
          <a href='#' > About us </a>
          <a href='#' > Tutorials  </a>
          <a href='#' > Resources </a>
          <a href='#' > Help Center </a>
          <a href='#' > Templates </a>
          <a href='#' > Case Studies </a>
        </div>

        <div className='links-column'>
          <a href='#'> Medium  </a>
          <a href='#' > Twitter  </a>
          <a href='#' > Facebook  </a>
          <a href='#' > Instagram </a>
          <a href='#' > LinkedIn </a>
        </div>

        <div className='links-column'>
          <a href='#' > Contact us </a>
          <Link to='/slack'> Slack </Link>
          <a href='#' > Jobs </a>
        </div>

      </div>
    </div>
  )
}

export default FooterLinks