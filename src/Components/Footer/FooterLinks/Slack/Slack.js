import React, {useState} from 'react'
import Navbar from "../../../Header/NavigationBar/Navbar";
import Button from "../../../../UI/Button/Button";
import { HuePicker } from 'react-color';


import './Slack.scss'

const Slack = () => {

  const [enteredText, setEnteredText] = useState('')
  const [content,setContent] = useState([])
  const [blockPickerColor, setBlockPickerColor] = useState("#37d67a");
  
  const inputHandler = (e) => {
    setEnteredText(e.target.value)
  }

  const listHandler = () => {
    setContent([...content,enteredText])

    setEnteredText('')
  }


  return (
    <div>

     <Navbar />


     <div className='input-button-container-slack'>
       <input className='input-slack' value={enteredText} onChange={inputHandler}/>
       <Button text='Add' onClick={listHandler}/>
     </div>

        <HuePicker
          className='color-picker'
          color={blockPickerColor}

          onChange={(color) => {
            setBlockPickerColor(color.hex);
          }}
        />


      <div className='content-list'>
        {
          content.map((item) => {
            return (
              <div className='li-container' style={{backgroundColor: `${blockPickerColor}`}}>
                <li className='li-item' > {item} </li>
              </div>
              )
          })
        }
      </div>

    </div>
  )
}

export default Slack