import React from 'react';
import './FooterRightsReserved.scss';


const FooterRightsReserved = () => {
  return (
    <div className='footer-text-wrapper'>
      <div className='text-1'> NinjaMail is a sample project for Quest AI. © 2019 Quest AI, All rights reserved. </div>
      <div className='text-2'> Terms & Conditions </div>
      <div className='text-3'> Privacy Policy </div>
    </div>
  )
}

export default FooterRightsReserved