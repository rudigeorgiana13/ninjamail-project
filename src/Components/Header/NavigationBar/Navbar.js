import React from 'react'
import {Link} from 'react-router-dom'
import imageIcon from '../../../UI/Icons/Navbar/mail (image).png'
import imageText from '../../../UI/Icons/Navbar/NinjaMail.png'

import './Navbar.scss'
import {useState} from 'react'
import Auth from "./Authentication/Auth";


const Navbar = (props) => {

  const [showAuth, setShowAuth] = useState(false)


  const authOpenModalHandler = () => {
    setShowAuth(true)
  }

  const authCloseModalHandler = () => {
    setShowAuth(false)
  }

  return (
    <div className='container'>
      
      <Link to='/'>

        {props.hideIcon !== true && (
          <div className='nav-icons'>
            <div> <img className='image-nav-icon' src={imageIcon}/> </div>
            <div className='icon-text'> <img className='text-nav-icon' src={imageText}/> </div>
          </div>
        )}

      </Link>

      <div className='nav-links'>
        <Link to='/features' className='features'> FEATURES </Link>
        <Link to='/pricing' className='pricing'> PRICING  </Link>
        <Link to='/services' className='services'> SERVICES </Link>
        <Link to='/partners' className='partners'> PARTNERS </Link>
        <div className='sign-up' onClick={authOpenModalHandler}>  SIGN UP FREE  </div>
      </div>
      { showAuth && <Auth onClose={authCloseModalHandler} />}
    </div>
  )
}


export default Navbar