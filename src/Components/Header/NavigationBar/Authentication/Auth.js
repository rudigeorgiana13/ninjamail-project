import React, {useEffect} from 'react'
import {useRef} from 'react'
import './Auth.scss'

const Auth = (props) => {
  const ref = useRef(null);


  const onClickOutside = (event) => {
    if (!ref?.current?.contains(event.target)) {
      props.onClose();
    }
  }

  useEffect(() => {
    document.addEventListener('click', onClickOutside)

    return () => document.removeEventListener('click', onClickOutside)
  })


  return (
      <div className='auth-container' ref={ref}>
        <div className='title-modal'>
          Hi there! Enter your name&surname and e-mail!
        </div>
        <div className='input-container'>
          <input className='input-name' placeholder='Name and Surname'/>
          <input className='input-email' placeholder='e-mail'/>
        </div>
        <button className='sign-up-modal-btn' onClick={props.onClose} > Sign up for free </button>
        <button className='close-the-modal sign-up-modal-btn' onClick={props.onClose}> Close </button>
      </div>
  )
}

export default Auth