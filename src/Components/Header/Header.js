import React from 'react'
import Navbar from "./NavigationBar/Navbar";
import WelcomingHeader from "./WelcomingHeader/WelcomingHeader";

const Header = () => {
  return (
    <div>
      <Navbar />
      <WelcomingHeader/>
    </div>
  )
}

export default Header