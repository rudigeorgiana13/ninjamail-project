import React from 'react'
import image from '../../../UI/Images/Header/photo-1544717302-de2939b7ef71 (image).png';

import Ellipse1 from '../../../UI/Images/Header/Figures/Ellipse 1.png';
import Ellipse6 from '../../../UI/Images/Header/Figures/Ellipse 6.png';
import Rectangle1 from '../../../UI/Images/Header/Figures/Rectangle 1.png';
import Rectangle2 from '../../../UI/Images/Header/Figures/Rectangle 2.png';
import Rectangle3 from '../../../UI/Images/Header/Figures/Rectangle 3.png';
import Rectangle9 from '../../../UI/Images/Header/Figures/Rectangle 9.png';
import Rectangle10 from '../../../UI/Images/Header/Figures/Rectangle 10.png';


import figures from '../../../UI/Images/Header/Figures/floating_elements.png'



import './WelcomingHeader.scss'

const WelcomingHeader = () => {
  return (
    <div className='header-container'>

    <div className='welcoming-container'>
      <div className='text-container'>
        <div className='header-title'> Create Stunning Email Campaigns</div>
        <div className='header-description'>
          Create and launch email campaigns
          that captivate  your customers in just a few clicks.
        </div>
        <div className='buttons-container'>
          <div className='try-now-button'> TRY NOW </div>
          <div className='get-a-demo-button'> GET A DEMO  </div>
        </div>
      </div>

      <div>
        <img className='header-image' src={image}/>
      </div>
    </div>

        <img className='ellipse1' src={Ellipse1} />
        <img className='ellipse6' src={Ellipse6} />
        <img className='ellipse7' src={Ellipse6} />
        <img className='ellipse8' src={Ellipse6} />
        <img className='ellipse9' src={Ellipse6} />
        <img className='rectangle1' src={Rectangle1} />
        <img className='rectangle2' src={Rectangle2} />
        <img className='rectangle3' src={Rectangle3} />
        <img className='rectangle9' src={Rectangle9} />
        <img className='rectangle10' src={Rectangle10} />

        {/*<img className='figures' src={figures}/>*/}


      </div>
  )
}

export default WelcomingHeader