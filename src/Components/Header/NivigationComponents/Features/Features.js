import React,{Component} from 'react'
import './Features.scss'
import Navbar from "../../NavigationBar/Navbar";
import Card from './Card'
import {data} from '../../../../Data/data'
import Slider from 'react-slick';
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";



const Features = (props) => {
  const settings = {
    dots: true,
    infinite: true,
    speed: 500,
    slidesToShow: 3,
    slidesToScroll: 3,
    autoplaySpeed: 2000,
    cssEase: "linear",
    autoplay: true,
    pauseOnHover: true,
    centerMode:true,
    touchMove:false
  };

  return (
    <div>
      <Navbar/>
        <div className='page-features-container'>
        <div className='cards-container'>


          <Slider {...settings} >
            { data.map(item => (
              <Card key={item.id} img={item.img} price={item.price} description={item.description} sold={item.sold}/>
            )) }
          </Slider>
        </div>

        </div>
    </div>
  )
}

export default Features