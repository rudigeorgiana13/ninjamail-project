import React from 'react'
import './Features.scss'


const Card = (props) => {
  return (
    <div className='card'>
      <div>
        <img
          className='image-card'
          src={props.img}
        />
      </div>
      <div className='description-card'> {props.description} </div>
      <div className='price'> {props.price} </div>
      <div className='sold'>  {props.sold} </div>
    </div>
  )
}

export default Card