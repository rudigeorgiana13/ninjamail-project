import React, {useState} from 'react'
import './Services.scss'
import Navbar from "../../NavigationBar/Navbar";

import {ImCross} from 'react-icons/im'
import Button from "../../../../UI/Button/Button";


const Services = (props) => {

  const [enteredService, setEnteredService] = useState('')
  const [services,setServices] = useState([])

  const serviceChangeHandler = (event) => {
    const trimmedValue = event.target.value.trim()
    const typedText = trimmedValue.length > 0 ? event.target.value : '';
    setEnteredService(typedText)
  }


  const buttonAddHandler = () => {
    setServices([...services, enteredService])

    setEnteredService('')
  }

  const removeService = (deletedItem) => {
    const newServices = services.filter((item,index) => item !== deletedItem)


    setServices([...newServices])
  }




  return (
   <>
     <Navbar hideIcon={true} />

    <div className='service-container'>
      <div className='input-button-container'>
        <input className='input-service' placeholder='Add a service...' value={enteredService} onChange={serviceChangeHandler} />
        <Button
          text={'Add'}
          onClick={buttonAddHandler}
          disabled={enteredService.length === 0}
        />
      </div>

      <div className='service-list'>
        {services.length > 0 ? (
          <div className='item-container'>
            {services.map((item) => (
              <div className='li-container'>
                <li className='li-item'> {item} </li>
                <ImCross className='icon-cross' onClick={() => removeService(item)}/>
              </div>
            ))}
          </div>
        ) : <p className='text-service'> No Service Available </p>}

      </div>

    </div>

     </>
  )
}

export default Services