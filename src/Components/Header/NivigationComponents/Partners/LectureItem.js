import React, {useState} from 'react'
import {ImCross} from 'react-icons/im'
import {AiTwotoneEdit} from 'react-icons/ai'
import {MdOutlineRemoveRedEye} from 'react-icons/md'
import {CgRadioChecked} from 'react-icons/cg'
import './Partners.scss'
import Button from "../../../../UI/Button/Button";
import ButtonClose from "../../../../UI/Button/Button-close";


const LectureItem = (props) => {

  const [enteredEditedTextPage, setEnteredEditedTextPage] = useState(props.item.pages)
  const [enteredEditedTextEmail, setEnteredEditedTextEmail] = useState(props.item.eMail)
  const [edit,setEdit] = useState(false)


  const handleInputChangePage = (e) => {
    setEnteredEditedTextPage(e.target.value)
  }

  const handleInputChangeEmail = (e) => {
    setEnteredEditedTextEmail(e.target.value)
  }

  const editLecture = (editedLecture) => {
    setEdit(true)
    console.log("edit mode activated")
  }

  const closeEditing = () => {
    setEdit(false)
    console.log('close edit')
  }

  const saveEditing = () => {
    setEdit(false)
    props.edit({
      ...props.item,
      pages: enteredEditedTextPage,
      eMail: enteredEditedTextEmail,
      edited: true,

    })
  }

  const borderHandler = () => {
    props.edit({
      ...props.item,
      border: !props.item.border,
    })
  }

 return (
   <div className={`li-lecture-container ${props.item.border ? 'background' : ''}`}>

       <div >
         <div> <CgRadioChecked className='radio' onClick={borderHandler} /> </div>

       { props.item.name &&
       <li className='li-studies'>
         <strong>Name:</strong> {props.item.name}
         <ImCross className='icon-li-lecture' onClick={props.remove}/>
         <AiTwotoneEdit  className='icon-li-lecture-edit' onClick={editLecture}/>
       </li>
       }

       {
         edit ? (
             <>
               <li className='li-age'><strong> Pages:</strong>
               <input
                 type='text'
                 className='styled-input-editing-pages'
                 value={enteredEditedTextPage}
                 onChange={handleInputChangePage}
               />
               </li>

               <li className='li-salary'> <strong>E-mail:</strong>
                 <input
                 type='text'
                 className='styled-input-editing-email'
                 value={enteredEditedTextEmail}
                 onChange={handleInputChangeEmail}
               />
               </li>

               <div className='btn-close-open'>
                 <ButtonClose text='Cancel' onClick={closeEditing} />
                 <Button text='Save' onClick={saveEditing} />
               </div>
             </>
           )
           : (
             <>
               {props.item.pages && <li className='li-age'><strong> Pages:</strong> {props.item.pages} pg. </li>}
               {props.item.eMail && <li className='li-salary'><strong>E-mail:</strong> {props.item.eMail}  </li>}
             </>
           )
       }
       </div>


       {props.item.edited  ?
         <div className='edited'>
           <MdOutlineRemoveRedEye className='edited-icon'/>
           <div className='edited-text'> Edited</div>
         </div>
         : ''
       }
     </div>
 )
}

export default LectureItem