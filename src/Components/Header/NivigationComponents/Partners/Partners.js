import React, {useState} from 'react';
import './Partners.scss';
import Button from "../../../../UI/Button/Button";
import ButtonClose from "../../../../UI/Button/Button-close";
import Navbar from "../../NavigationBar/Navbar";
import LectureItem from "./LectureItem";
import {guidGenerator} from "../../../../helpers/generator";



const Partners = (props) => {

  const [showForm, setShowForm] = useState(false)
  const [enteredText1, setEnteredText1] = useState('')
  const [enteredText2, setEnteredText2] = useState('')
  const [enteredText3, setEnteredText3] = useState('')
  const [content, setContent] = useState([])


  const onClickToOpenFormHandler = () => {
   setShowForm(true)
  }

  const onClickToCloseFormHandler = () => {
   setShowForm(false)

  }

  const input1ContentHandler = (e) => {
    setEnteredText1(e.target.value)
  }

  const input2ContentHandler = (e) => {
    setEnteredText2(e.target.value)
  }

  const input3ContentHandler = (e) => {
    setEnteredText3(e.target.value)
  }

  const onClickToAddContent = () => {
    setContent([...content, {
      id: guidGenerator(),
      name: enteredText1.charAt(0).toUpperCase()+enteredText1.slice(1),
      pages: enteredText2,
      eMail: enteredText3,
      edited: false,
      border:false,
    }] )

    setEnteredText1('')
    setEnteredText2('')
    setEnteredText3('')
  }

  const removeLecture = (removedLecture) => {
    const newContentLecture = content.filter(item => item.id !== removedLecture.id)

    setContent(newContentLecture)
  }

  console.log('Content:', content)

  const editLecture = (editedLecture) => {
    console.log('Edited lecture',editedLecture)
    const newLectures = content.map(item => {
      if(item.id === editedLecture.id) {
        return editedLecture
      }
       return item
    })

    setContent(newLectures);
  }


  return (
    <>
      <Navbar />
       <div className='form-container'>

      <div className='title-button-container'>
        <h1> Insert lecture </h1>
        {!showForm  && <Button text={'Open form'} className='open-button' onClick={ () => onClickToOpenFormHandler()}/> }
      </div>
         {showForm ?

           <div className='inputs-container-lecture'>
             <input
               className='input-name-lecture'
               placeholder='Name'
               value={enteredText1}
               onChange={input1ContentHandler}
             />

             <input
               className='input-pages-lecture'
               placeholder='Pages'
               value={enteredText2}
               onChange={input2ContentHandler}
               type='number'
             />

             <input
               className='input-email-lecture'
               placeholder='E-mail'
               value={enteredText3}
               onChange={input3ContentHandler}
             />

             <div className='btn-close-open'>
               <ButtonClose text={'Close'} onClick={() => onClickToCloseFormHandler()}/>
               <Button  text={'Send'} onClick={() => onClickToAddContent()} disabled={enteredText1.length===0} />
             </div>
           </div>
         : '' }
    </div>



      <div className='content'>
        {content.map(item => (
          <LectureItem
            item={item}
            remove={() => removeLecture(item)}
            edit={editLecture}
          />
          ))}
      </div>


      </>
  )
}

export default Partners