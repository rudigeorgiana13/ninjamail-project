import React from 'react'
import {useState} from 'react'
import './Pricing.scss'
import Navbar from "../../NavigationBar/Navbar";
import Button from "../../../../UI/Button/Button";
import {ImCross} from "react-icons/im";
import {guidGenerator} from "../../../../helpers/generator";


const Pricing = () => {

  const [enteredText1, setEnteredText1] = useState('')
  const [enteredText2, setEnteredText2] = useState('')
  const [enteredText3, setEnteredText3] = useState('')
  const [addElements, setAddElements] = useState([])


  const input1ContentHandler = (e) => {
    setEnteredText1(e.target.value)
  }

  const input2ContentHandler = (e) => {
    setEnteredText2(e.target.value)
  }

  const input3ContentHandler = (e) => {
    setEnteredText3(e.target.value)
  }

  const buttonAddHandler = () => {
    setAddElements([...addElements, {
      studies: enteredText1,
      age: enteredText2,
      wantedSalary: enteredText3,
      id: guidGenerator(),
    }])

    setEnteredText1('')
    setEnteredText2('')
    setEnteredText3('')
  }


  const removePosition = (deletedItem) => {
    const newPosition = addElements.filter((item) => item.id !== deletedItem.id)

    setAddElements([...newPosition])
    console.log(deletedItem)

  }


  console.log(addElements)

  return (
    <>
        <Navbar />
    <div className='job-container'>
      <p className='title-job'> Insert your position  </p>
      <div className='inputs-container'>
        <input
          className='input-studies'
          placeholder='Insert your studies'
          value={enteredText1}
          onChange={input1ContentHandler}
        />

        <input
          className='input-age'
          placeholder='Insert your age'
          type='number'
          value={enteredText2}
          onChange={input2ContentHandler}
        />

        <input
          className='input-salary'
          placeholder='Insert the salary you want'
          value={enteredText3}
          type='number'
          onChange={input3ContentHandler}
        />

      </div>

      <div>
        <Button
          text={'Add'}
          onClick={buttonAddHandler}
          disabled={
            enteredText1.length === 0 ||
            enteredText3.length === 0  }
        />
      </div>
      <div className='get-info-container'>
        {addElements.length > 0 ?
          <div>
            {addElements.map((item) => (
              <>
                <div className='li-job-container' >
                  <li className='li-studies'> Studies: {item.studies} </li>
                  { item.age && (<li className='li-age'> Age: {item.age} age </li>) }
                  <li className='li-salary'> Salary: {item.wantedSalary} $ </li>
                  <ImCross className='close-icon' onClick={() => removePosition(item)} />
                </div>
              </>
            ))}
          </div>
          : <p className='title-job-available'> No jobs are available ... </p>
        }

      </div>
    </div>

    </>
  )
}

export default Pricing