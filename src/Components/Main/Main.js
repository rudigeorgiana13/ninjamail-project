import React from 'react'
import MainCostumers from "./MainCostumers/MainCostumers"
import MainEmail from "./MainEmail/MainEmail";
import MainBrands from "./MainBrands/MainBrands";



const Main = () => {

  return (
    <div>
      <MainCostumers />
      <MainEmail />
      <MainBrands />
    </div>
  )
};


export default Main