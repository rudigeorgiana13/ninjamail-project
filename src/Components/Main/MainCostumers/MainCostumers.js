import React from 'react'
import {useState} from 'react'
import image from '../../../UI/Images/Main/image.png'
import greenBg from '../../../UI/Images/Main/green-bg.png'
import girlImg from '../../../UI/Images/Main/photo.png'
import groupImg from '../../../UI/Images/Main/photo (1).png'
import lines from '../../../UI/Images/Main/lines-white.png'

import {RiArrowDropRightLine} from 'react-icons/ri'
import './MainCostumers.scss'



import VideoModal  from "../../../Modal/VideoModal";


const MainCostumers = () => {

 const [showModal, setShowModal] = useState(false)


  const openModalHandler = () => {
   setShowModal(true)
  }

  const closeModalHandler = () => {
   setShowModal(false)
  }

  return (
    <div>

      <div className='main-container' >
        <div className='text-button-container' style={{ backgroundImage: `url(${image})` }} >
          <div className='text-main'>Reach More Customers</div>
          <div className='button-learn-how' onClick={openModalHandler}> LEARN HOW </div>
          {showModal && (<VideoModal onClose={closeModalHandler} />)}
        </div>
      </div>


      <div className='people-main-container'>
        <div className='card1'>
          <div> <img  className='img-main-girl' src={girlImg}/></div>
          <div className='card-text-1'>
            Launch campaigns but also find new customers. Our unique platform handles campaigns from start to end.
          </div>
          <div className='button-learn-more-1'>
            <div> Learn More </div>
            <div> <RiArrowDropRightLine className='arrow-icon'/> </div>
          </div>
        </div>

        <div className='card2'>
          <div> <img className='img-main-group' src={groupImg}/></div>
          <div className='card-text-2'>
            Start building and sharing with your team today. NinjaMail is renowned for its industry leading team
            collaboration tools.
          </div>
          <div className='button-learn-more-2'>
            <div> Learn More </div>
            <div> <RiArrowDropRightLine className='arrow-icon'/> </div>
          </div>
        </div>

        <div className='cards-description'>
          <div className='title'> The source for proven, engaging email campaigns</div>
          <div className='description'>
            Whether you’re a startup, small business, e-commerce store, or want to promote your app,
            NinjaMail has the feature set tailored for your business.
          </div>
        </div>
      </div>

      <img className='green-bg-image' src={greenBg}/>
      <img className='lines' src={lines} />
    </div>



)
}

export default MainCostumers