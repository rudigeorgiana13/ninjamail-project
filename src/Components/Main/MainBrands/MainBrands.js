import React from 'react'
import lastKnight from '../../../UI/Images/Main/last-knight.png'
import dragon from '../../../UI/Images/Main/dragon-eye.png'
import quest from '../../../UI/Images/Main/Quest.png'
import radio from '../../../UI/Images/Main/radio-tee.png'
import game from '../../../UI/Images/Main/game-commerce.png'


import './MainBrands.scss'



const MainBrands = () => {
  return (
    <div>

      <div className='brands-container'>
        <div className='brands-title'> All the best brands already use us. </div>
        <div className='logo-container'>
          <div> <img className='logo-lastKnight' src={lastKnight}/> </div>
          <div> <img className='logo-dragon' src={dragon}/> </div>
          <div> <img className='logo-quest' src={quest}/> </div>
          <div> <img className='logo-radio' src={radio}/> </div>
          <div> <img className='logo-game' src={game}/> </div>
        </div>
      </div>

      <div className='get-started-container'>
        <div className='get-started-title'> Get started today! </div>
        <div className='pick-a-plan-button'> PICK A PLAN </div>
      </div>

    </div>
  )
}

export default MainBrands