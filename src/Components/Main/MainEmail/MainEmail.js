import React from 'react'
import './MainEmail.scss'

import frankieImg from '../../../UI/Images/Main/Frankie.png'
import camilleImg from '../../../UI/Images/Main/camille.png'
import elayneImg from '../../../UI/Images/Main/elayne.png'


const MainEmail = () => {
  return (
    <div className='main-photo-input-container'>
      <div className='photo-container'>
        <div className='frankie-container'>
          <img className='frankie-img' src={frankieImg}/>
          <div className='frankie-name'> Frankie </div>
          <div className='frankie-description'> Member since 2016 </div>
        </div>

        <div className='camille-container'>
          <img className='camille-img' src={camilleImg}/>
          <div className='camille-name'> Camile </div>
          <div className='camille-description'> Member since 2012 </div>
        </div>

        <div className='elayne-container'>
          <img className='elayne-img' src={elayneImg}/>
          <div className='elayne-name'> Elayne</div>
          <div className='elayne-description'> Member since 2018 </div>
        </div>
      </div>


      <div className='title-reaching'>
        Learn how others are reaching their audience easier than ever before.
      </div>

      <div className='input-join-container'>
        <div> <input className='input' placeholder='Enter your email'/> </div>
        <div className='button-join-our-list'> JOIN OUR LIST </div>
      </div>

    </div>
  )
}

export default MainEmail