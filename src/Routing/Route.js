import React from 'react'
import {Routes,Route} from "react-router-dom";

import Home from "../Components/Home";
import Features from "../Components/Header/NivigationComponents/Features/Features";
import Pricing from "../Components/Header/NivigationComponents/Pricing/Pricing";
import Services from "../Components/Header/NivigationComponents/Services/Services";
import Partners from "../Components/Header/NivigationComponents/Partners/Partners";
import Slack from "../Components/Footer/FooterLinks/Slack/Slack";


const Routing = () => {

return (
 <Routes>

   <Route path='/' element={<Home/> } />
   <Route path='/features' element={<Features/>} />
   <Route path='/pricing' element={<Pricing/>}/>
   <Route path='/services' element={<Services/>}/>
   <Route path='/partners' element={<Partners/>}/>
   <Route path='/slack' element={<Slack/>} />

 </Routes>
)
}

export default Routing