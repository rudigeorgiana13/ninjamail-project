import React from 'react'
import './Button.scss'



const ButtonClose = (props) => {

  return (
    <div className='btn-close' onClick={props.onClick} >
      {props.text}
    </div>
  )
}

export default ButtonClose
