import React from 'react'
import './Button.scss'



const Button = (props) => {

  const buttonOnClick = () => {
    if(props.disabled !== true) {
      return props.onClick && props.onClick()
    }
  }

  return (
    <div className={` btn ${props.disabled ? 'disabled' : ''} ` } onClick={buttonOnClick} >
      {props.text}
    </div>
  )
}

export default Button

