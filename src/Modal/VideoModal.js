import React from 'react';
import './VideoModal.scss';
import video from '../UI/video/video-learn-how.mp4';


const VideoModal = (props) => {


  return (
    <div className='modal-container'>
      <div className='video-modal-container'>
        <video controls className='video'>
          <source src={video} />
        </video>
      </div>

      <button className='close-btn' onClick={props.onClose}>
        Close
      </button>
    </div>
  )
}

export default VideoModal